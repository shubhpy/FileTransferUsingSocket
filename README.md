1. Run server.py in background.
	
    ```python server.py nohup &```

2. In nginx server block, add location of files.
    
    ```
    server {
        client_max_body_size 200m;
        client_body_timeout 1000s;
        listen 80;
        server_name xx.com;

        location /files/ {
            proxy_pass_header Server;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Scheme $scheme;
            proxy_pass http://172.17.0.xx:8080;
        }
	}
	```
	
3. save the server block in sites-enabled of nginx and restart nginx.

4. Visit xx.com/files/ to see files hosted by you.

5. Working on runner Gitlab CI

6. Working on how local image
